﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StoryAnimation : MonoBehaviour {

	public Animator anim;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (anim.GetCurrentAnimatorStateInfo (0).IsName ("NextLvl"))
			SceneManager.LoadScene ("Vault");
	}
}
