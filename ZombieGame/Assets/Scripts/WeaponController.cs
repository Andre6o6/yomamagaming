﻿using UnityEngine;
using System.Collections;

public class WeaponController : MonoBehaviour
{
	public enum WpnNames {ak, pistol, shotgun, twoBarreled, usi, sniper, crossbow, bat, knife};
	public WpnNames wName;
    public int damage;
    public float rateOfFire; //
    public int accuracy; //в процентах
    public int ammoCapacity;
	public int projectileCount;
    public GameObject projectile;
    public bool automatic;
	public bool twoHanded;
	public bool melee;			//melee or ranged
    public float reloadTime;

	//Close Combat
	public int combatDmg;
	public int curCombatDmg;
	public float hitRate;
	//public GameObject hitProjectile;
	bool canHit;

    public AudioClip shootSound;
    public AudioClip zeroAmmoCLickSound;
    public AudioClip reloadSound;
    public AudioClip shutterSound;
    public AudioClip hitSound;

    AudioSource source;
    float volLowRange = .5f;
    float volHighRange = 1.0f;
    bool clickFlag = false;

	public Sprite worldSprite;
	public Sprite inhandSprite;

	GameObject instProjectile;
    int currentAmmo;
    float projectileShift;
    float passedTime;
    bool canShoot;
	public bool reloading = false;
	public LineRenderer laser;

	Vector3 laserStart;
	float spHeight;

    void Start()
    {
        source = GetComponent <AudioSource>();

        currentAmmo = ammoCapacity;

		if (!melee) 
		{
			RecalculateProjectileShift ();
		}
    }

    void Update()
    {
		if (laser != null && Input.GetMouseButton (1)) {
			RaycastHit2D hit;
			laser.enabled = true;
			laserStart = transform.position + transform.up * projectileShift;
			hit = Physics2D.Raycast ((Vector2)laserStart, -(Vector2)(transform.position - laserStart), 1000);

			if (hit.collider != null) {
				laser.SetPositions (new Vector3[]{ laserStart, hit.point });
				Debug.Log ("a");
			} else {
				laser.SetPositions (new Vector3[]{ laserStart, -(transform.position - laserStart) * 100 + laserStart });
				Debug.Log ("B");
			}
		} else if (laser != null && Input.GetMouseButtonUp (1)) {
			laser.enabled = false;
		}

		if (!canShoot && passedTime < 1 / rateOfFire)
        {
            passedTime += Time.deltaTime;
        }
		else if (!reloading && currentAmmo != 0)
        {
            canShoot = true;
        }

		if (!canHit && passedTime < 1 / hitRate)
		{
			passedTime += Time.deltaTime;
		}
		else
		{	
			canHit = true;
		}

		//clickFlag = false;

		if(currentAmmo == 0)
        {
            //canShoot = false;*/

			if (Input.GetMouseButtonDown(0) && clickFlag && !reloading)	
			{
				source.PlayOneShot(zeroAmmoCLickSound, 1F);
				//clickFlag = false;
			}

			if (clickFlag == false) {
				clickFlag = true;
			}
        }
    }

	void OnCollisionEnter2D(Collision2D other)					
	{
		if (tag == "Wpn" && other.gameObject.tag == "Enemy" && !canHit)
		{
			other.gameObject.GetComponent<EnemyController> ().health -= curCombatDmg;
			curCombatDmg = 0;
		}
	}

	public bool Hit (float dmgMultiplier) {
		if (!canHit) {
			return false;
		}

        source.PlayOneShot(hitSound, 0.3F);	
		curCombatDmg = Mathf.RoundToInt (combatDmg * dmgMultiplier);

		passedTime = 0f;
		canHit = false;

		return true;
	}


    public bool Fire()
    {
		if (!canShoot || !canHit)
		{
            return false;
        }
			
		float vol = Random.Range(volLowRange, volHighRange);
        source.PlayOneShot(shootSound, vol);
        
		passedTime = 0f;
		canShoot = false;

		if (melee) {
			curCombatDmg = combatDmg;
			return true;
		}

        Vector3 shootingPoint = new Vector3(transform.position.x,
                                            transform.position.y,
                                            0f);

		float range;
		float angle;

		for (int i = 0; i < projectileCount; i++) {
			range = 60f * (1 - (float)accuracy / 100);
			angle = Random.Range(-range, range);
			((GameObject)Instantiate(projectile, shootingPoint + transform.up * projectileShift, Quaternion.Euler(0, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z) *  Quaternion.Euler (0f, 0f, angle))).GetComponent<ProjectileController>().damage = damage;
		}
		currentAmmo--;

		if (PlayerController.instance.noise < 100)
			PlayerController.instance.noise = 100;

        return true;
    }

	public IEnumerator Reload()	
	{
		if (currentAmmo == ammoCapacity || reloading)
		{
			yield break;
		}

		canShoot = false;
		reloading = true; 	
		PlayerController.instance.mainAnim.SetBool ("Reloading", true);

		if ((wName == WpnNames.shotgun) || (wName == WpnNames.twoBarreled))
        {
            for (int i = 0; i < ammoCapacity - currentAmmo; ++i)
            {
                source.PlayOneShot(reloadSound, 1F);
                yield return new WaitForSeconds(0.62f);
            }
            source.PlayOneShot(shutterSound, 1F);
            yield return new WaitForSeconds(0.8f);
        }
        else
        {
            if (wName == WpnNames.crossbow)
            {
                source.PlayOneShot(reloadSound, 0.08F);
                yield return new WaitForSeconds(reloadTime);
            }
            else
            {
                source.PlayOneShot(reloadSound, 1F);
                yield return new WaitForSeconds(reloadTime);
            }
        }
		currentAmmo = ammoCapacity;
		canShoot = true;
		clickFlag = false;
		reloading = false;	
		PlayerController.instance.mainAnim.SetBool ("Reloading", false);
	}

	public void RecalculateProjectileShift()
	{
		SpriteRenderer weaponRenderer = GetComponent<SpriteRenderer>();
		SpriteRenderer projectileRenderer = projectile.GetComponent<SpriteRenderer>();
		projectileShift = weaponRenderer.sprite.rect.height / (2 * weaponRenderer.sprite.pixelsPerUnit) +
			projectileRenderer.sprite.rect.height / (2 * projectileRenderer.sprite.pixelsPerUnit);
	}
}
