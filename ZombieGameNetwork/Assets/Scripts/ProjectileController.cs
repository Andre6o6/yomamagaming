﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class ProjectileController : NetworkBehaviour
{
    public float overLifetime;
    public float velocity;
    public int damage;

	// Use this for initialization
	void Start ()
    {
        Destroy(gameObject, overLifetime);
	}

    void Update()
    {
        transform.Translate(Vector3.up * velocity * Time.deltaTime);
    }


	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag != "Enemy" && other.gameObject.name !="Lake" && other.gameObject.tag != "Projectile" && other.gameObject.tag != "PickableWpn") 
		{
			Destroy (gameObject);
		}
	}
}
