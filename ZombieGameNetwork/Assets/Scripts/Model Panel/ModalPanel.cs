﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System;

public class ModalPanel : MonoBehaviour {

    public Text question;
    public Image iconImage;
    public Image iconImage2;
    public Button FirstButton;
    public Button SecondButton;
    public Button ThirdButton;
    public Button FourthButton;
    public GameObject modalPanelObject;


    private static ModalPanel[] modalPanel;
    public static ModalPanel[] Instance()
    {
        //if (!modalPanel)
        //{
        //    modalPanel = FindObjectOfType(typeof(ModalPanel)) as ModalPanel;
        //    if (!modalPanel)
        //        Debug.LogError("There needs to be one active ModalPanel script on a GameObject in your scene.");
        //}

        //return modalPanel;
        if (modalPanel == null)
        {
            modalPanel = FindObjectsOfType(typeof(ModalPanel)) as ModalPanel[];
            if (modalPanel == null)
                Debug.LogError("There needs to be one active ModalPanel script on a GameObject in your scene.");
        }

        return modalPanel;
    }

    public void ChoiceIngameMenu(string question, UnityAction resumeEvent, UnityAction restartEvent, UnityAction optionEvent, UnityAction quitEvent)
    {
        modalPanelObject.SetActive(true);

        FirstButton.onClick.RemoveAllListeners();
        FirstButton.onClick.AddListener(resumeEvent);
        FirstButton.onClick.AddListener(ClosePanel);

        SecondButton.onClick.RemoveAllListeners();
        SecondButton.onClick.AddListener(restartEvent);
        SecondButton.onClick.AddListener(ClosePanel);

        ThirdButton.onClick.RemoveAllListeners();
        ThirdButton.onClick.AddListener(optionEvent);
        ThirdButton.onClick.AddListener(ClosePanel);

        FourthButton.onClick.RemoveAllListeners();
        FourthButton.onClick.AddListener(quitEvent);
        FourthButton.onClick.AddListener(ClosePanel);

        //this.question.text = question;

        this.iconImage.gameObject.SetActive(false);
        FirstButton.gameObject.SetActive(true);
        SecondButton.gameObject.SetActive(true);
        ThirdButton.gameObject.SetActive(true);
        FourthButton.gameObject.SetActive(true);
    }

    public void Choice(string question, UnityAction yesEvent, UnityAction noEvent, UnityAction cancelEvent)
    {
        modalPanelObject.SetActive(true);

        FirstButton.onClick.RemoveAllListeners();
        FirstButton.onClick.AddListener(yesEvent);
        FirstButton.onClick.AddListener(ClosePanel);

        SecondButton.onClick.RemoveAllListeners();
        SecondButton.onClick.AddListener(noEvent);
        SecondButton.onClick.AddListener(ClosePanel);

        ThirdButton.onClick.RemoveAllListeners();
        ThirdButton.onClick.AddListener(cancelEvent);
        ThirdButton.onClick.AddListener(ClosePanel);

        this.question.text = question;

        this.iconImage.gameObject.SetActive(false);
        FirstButton.gameObject.SetActive(true);
        SecondButton.gameObject.SetActive(true);
        ThirdButton.gameObject.SetActive(true);
    }

    public void Choice(UnityAction yesEvent, UnityAction noEvent, UnityAction cancelEvent)
    {
        modalPanelObject.SetActive(true);

        FirstButton.onClick.RemoveAllListeners();
        FirstButton.onClick.AddListener(yesEvent);

        SecondButton.onClick.RemoveAllListeners();
        SecondButton.onClick.AddListener(noEvent);

        ThirdButton.onClick.RemoveAllListeners();
        ThirdButton.onClick.AddListener(ClosePanel);
        ThirdButton.onClick.AddListener(cancelEvent);

		/*try
		{
			this.iconImage.gameObject.SetActive(false);
		}
		catch(NullReferenceException e)
		{
			Debug.Log ("эй, ты пикчу забыл поставить");	
		}*/

        FirstButton.gameObject.SetActive(true);
        SecondButton.gameObject.SetActive(true);
        ThirdButton.gameObject.SetActive(true);
    }

    public void Choice(string question, UnityAction yesEvent, UnityAction noEvent)
    {
        modalPanelObject.SetActive(true);

        FirstButton.onClick.RemoveAllListeners();
        FirstButton.onClick.AddListener(yesEvent);
        FirstButton.onClick.AddListener(ClosePanel);

        SecondButton.onClick.RemoveAllListeners();
        SecondButton.onClick.AddListener(noEvent);
        SecondButton.onClick.AddListener(ClosePanel);

        this.question.text = question;

        this.iconImage.gameObject.SetActive(false);

        FirstButton.gameObject.SetActive(true);
        SecondButton.gameObject.SetActive(true);
    }

    public void JustPanel(string question, UnityAction yesEvent)
    {
        modalPanelObject.SetActive(true);

        FirstButton.onClick.RemoveAllListeners();
        FirstButton.onClick.AddListener(ClosePanel);
        FirstButton.onClick.AddListener(yesEvent);


        this.question.text = question;

        FirstButton.gameObject.SetActive(true);
    }

    public void JustPanel(UnityAction yesEvent)
    {
        modalPanelObject.SetActive(true);

        FirstButton.onClick.RemoveAllListeners();
        FirstButton.onClick.AddListener(yesEvent);
        FirstButton.onClick.AddListener(ClosePanel);

		try
		{
			this.iconImage.gameObject.SetActive(false);
		}
		catch(NullReferenceException e)
		{
			Debug.Log ("эй, ты пикчу забыл поставить");	
		}

		FirstButton.gameObject.SetActive(true);
    }

    void ClosePanel()
    {
        modalPanelObject.SetActive(false);
    }

	void OnDestroy()
	{
		modalPanel = null;
	}
}
