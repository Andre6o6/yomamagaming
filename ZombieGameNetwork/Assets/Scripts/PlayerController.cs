﻿using UnityEngine;
using System.Collections;
//using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour
{
	private static PlayerController _instance;
	public static PlayerController instance
	{
		get
		{
			if (_instance == null) Debug.LogError("No player found");
			return _instance;
		}
	}

    public float velocity;
    public GameObject playerImage;
    public GameObject head;
    public Animator walkingAnim;
	public Animator mainAnim;
    public WeaponController weapon;
	public WeaponController primaryWpn;
	public WeaponController secondaryWpn;

	public IngameMenuUI IngameMenu;

	public float noise;

    public int maxHealth;
    public int health;
	public int stamina;
	float dmgMlt = 1f;
		
//Мб структурой?

	public int medkits;
	int ammo;	//?
	public int food;
	int fuel;
	int junk;	//?

    bool result;
    Camera mainCamera;
    Vector3 mouseDirection;
	Vector2 velocityDirection;
	Rigidbody2D rigidbody2d;
    float currentVelocity;
    float maxVelocity;
    float sign;
	Collider2D[] cols;	//массив, используемый для кэширования коллайдеров у подбираемого оружия

	void Awake ()
    {
		_instance = this;

        mainCamera = Camera.main;
        maxVelocity = 3 * velocity;
        health = maxHealth;
		rigidbody2d = GetComponent<Rigidbody2D> ();
		weapon = primaryWpn;
		secondaryWpn.gameObject.SetActive (false);
    }

	void Start()
	{
		LevelTransition.OnCheckpointSave += new LevelTransition.Action (OnSave);
		LevelTransition.OnCheckpointLoad += new LevelTransition.Action (OnLoad);
		LevelTransition.LoadCheckpoint ();
	}

	void OnTriggerStay2D (Collider2D other)
	{
		if (other.gameObject.tag == "PickableWpn") 
		{
			if (Input.GetKeyDown(KeyCode.E)) 
			{
				PickWpn (other.gameObject);
			}
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "enemyProjectile")
		{
			health -= other.gameObject.GetComponent<ProjectileController>().damage;
			Destroy(other.gameObject);
		}
	}

    void Update()
    {
		if (!isLocalPlayer) {
			return;
		}

		if (Input.GetKeyDown (KeyCode.Delete)) {	//надо бы это куда нибудь засунуть
			PlayerPrefs.DeleteAll ();
		}

		if (health > 0)
		{
			//Поворот за курсором
			mouseDirection = Input.mousePosition - mainCamera.WorldToScreenPoint (transform.position);
			sign = (Vector3.Angle (mouseDirection, Vector3.right) > 90f) ? 1.0f : -1.0f;
			transform.rotation *= Quaternion.Euler (0f, 0f, Vector3.Angle (mouseDirection, Vector3.up) * sign - transform.rotation.eulerAngles.z);

			//Передвижение
			float h = Input.GetAxisRaw("Horizontal");
			float v = Input.GetAxisRaw ("Vertical");
			Move (h, v);

			//Стрельба
            if ((Input.GetMouseButton(0) && weapon.automatic) || Input.GetMouseButtonDown(0))
            {
                if (!weapon.melee)
                {
                    CmdFire();
                    if (result)
                        mainAnim.SetTrigger("Shooting");
                }
                else
                {
                    CmdHit();
                    if (result)
                        mainAnim.SetTrigger("Hitting");
                }
            }
			if (Input.GetKeyDown (KeyCode.R))
			{
				StartCoroutine (weapon.Reload());
			}
			//ББ
			if (Input.GetKey (KeyCode.V)) {
				dmgMlt += Time.deltaTime;
			}
			if (Input.GetKeyUp (KeyCode.V)) {
                CmdHit();
				if (result) mainAnim.SetTrigger ("Hitting");
				dmgMlt = 1f;
			}

			//Смена оружия
			if (Input.GetKeyDown (KeyCode.Alpha1) && !weapon.reloading)
			{
				primaryWpn.gameObject.SetActive (true);
				weapon = primaryWpn;
				secondaryWpn.gameObject.SetActive (false);
				mainAnim.SetBool ("twoHanded", true);
			}
			if (Input.GetKeyDown (KeyCode.Alpha2) && !weapon.reloading)
			{
				secondaryWpn.gameObject.SetActive (true);
				weapon = secondaryWpn;
				primaryWpn.gameObject.SetActive (false);
				mainAnim.SetBool ("twoHanded", false);
			}

			//Звук
			if (noise > 1) noise = noise / Mathf.Pow(2, Time.deltaTime);  	
		}
		else 
		{
			Die ();
		}
    }

	[Command]
    public void CmdFire(){
		result = weapon.Fire ();
	}

	[Command] //Не знаю, нужно ли это. Пусть будет пока 
    public void CmdHit(){
		result = weapon.Hit (dmgMlt);
	}

	void Move (float h, float v)
	{
		velocityDirection = new Vector2 (h, v).normalized ;

		currentVelocity = 1.7f * velocity;		
		if (Input.GetKey (KeyCode.LeftShift)) 
		{
			currentVelocity = 0.7f * velocity;
		}

		if (rigidbody2d.angularVelocity != 0f) 
		{
			rigidbody2d.angularVelocity = 0f;
		}

		rigidbody2d.velocity = velocityDirection * currentVelocity;

		if (velocityDirection != Vector2.zero)
		{
			mainAnim.SetFloat ("Velosity", currentVelocity);
			walkingAnim.SetFloat ("Velosity", currentVelocity);

			float curNoiseLvl = currentVelocity;	//хорошо бы придумать чего поумнее
			if (noise < curNoiseLvl)
				noise = curNoiseLvl;
		}
		else
		{
			mainAnim.SetFloat ("Velosity", 0f);
			walkingAnim.SetFloat ("Velosity", 0f);

			if (rigidbody2d.velocity != Vector2.zero) 
			{
				rigidbody2d.velocity = Vector2.zero;
			}
		}
	}

	void PickWpn(GameObject other)
	{
		if (weapon.reloading)
			return;					

		other.tag = "Wpn";

		cols = other.GetComponents<Collider2D>();
		for (int i = 0; i < 2; i++) {
			if (cols [i].isTrigger == true)
				cols [i].enabled = false;
			else
				cols [i].enabled = true;
		}

		other.GetComponent<SpriteRenderer> ().sprite = other.GetComponent<WeaponController> ().inhandSprite;

		other.transform.SetParent (primaryWpn.transform.parent);
		WeaponController newWpn = other.GetComponent<WeaponController> ();

		if (newWpn.twoHanded) 
		{
			primaryWpn.gameObject.SetActive (true);
			primaryWpn.transform.SetParent (null);
			primaryWpn.transform.position = new Vector3 (transform.position.x, transform.position.y, 0.02f);
			primaryWpn.gameObject.tag = "PickableWpn";
			primaryWpn.GetComponent<SpriteRenderer> ().sprite = primaryWpn.GetComponent<WeaponController> ().worldSprite;

			cols = primaryWpn.GetComponents<Collider2D>();
			for (int i = 0; i < 2; i++) {
				if (cols [i].isTrigger == true)
					cols [i].enabled = true;
				else
					cols [i].enabled = false;
			}
				
			primaryWpn = newWpn;
			weapon = primaryWpn;
			secondaryWpn.gameObject.SetActive (false);
			other.transform.localPosition = new Vector3 (0.62f, 0.81f, -0.05f);
			other.transform.localRotation = new Quaternion ();
			mainAnim.SetBool ("twoHanded", true);

		} 
		else
		{
			secondaryWpn.gameObject.SetActive (true);
			secondaryWpn.transform.SetParent (null);
			secondaryWpn.transform.position = new Vector3 (transform.position.x, transform.position.y, 0.02f);
			secondaryWpn.gameObject.tag = "PickableWpn";
			secondaryWpn.GetComponent<SpriteRenderer> ().sprite = secondaryWpn.GetComponent<WeaponController> ().worldSprite;

			cols = secondaryWpn.GetComponents<Collider2D>();
			for (int i = 0; i < 2; i++) {
				if (cols [i].isTrigger == true)
					cols [i].enabled = true;
				else
					cols [i].enabled = false;
			}

			secondaryWpn = newWpn;
			weapon = secondaryWpn;
			primaryWpn.gameObject.SetActive (false);
			other.transform.localPosition = new Vector3 (0.62f, 1.48f, -0.05f);
			other.transform.localRotation = new Quaternion ();
			mainAnim.SetBool ("twoHanded", false);
		}

		if (!weapon.melee) 
		{
			weapon.RecalculateProjectileShift ();
		}
	}

	void OnDestroy()
	{
		_instance = null;
	}

	void Die()
	{
		mainAnim.SetTrigger ("Die");

		Destroy (gameObject.GetComponent<Collider2D>());
		this.enabled = false;
	}

	//Сохранение и загрузка
	void OnSave()		
	{
		PlayerPrefs.SetFloat ("playerPosX", transform.position.x);
		PlayerPrefs.SetFloat ("playerPosY", transform.position.y);
		PlayerPrefs.SetInt ("playerHealth", health);
		PlayerPrefs.SetString ("playerScene", SceneManager.GetActiveScene().name);	// будем ли сохраняться посреди сцены?
		//TODO : обрабатывать имена
		PlayerPrefs.SetString ("player1Wpn", primaryWpn.name); 
		PlayerPrefs.SetString ("player2Wpn", secondaryWpn.name);
	}

	void OnLoad()
	{
		if (!PlayerPrefs.HasKey ("playerPosX"))
			return;
		
		Vector3 pos = Vector3.zero;
		if (PlayerPrefs.GetString ("playerScene") == SceneManager.GetActiveScene ().name) {
			pos.x = PlayerPrefs.GetFloat ("playerPosX");
			pos.y = PlayerPrefs.GetFloat ("playerPosY");
			transform.position = pos;
		} else {
			//найти все объекты переходов
			//выбрать тот, который исходит из сцены из PlayerPrefs
			//загрузиться в его координатах
		}

		health = PlayerPrefs.GetInt ("playerHealth");
		var wpn1 = Instantiate ((GameObject)Resources.Load ("Wpn/" + PlayerPrefs.GetString("player1Wpn")));
		var wpn2 = Instantiate ((GameObject)Resources.Load ("Wpn/" + PlayerPrefs.GetString("player2Wpn")));
		PickWpn (wpn1);
		PickWpn (wpn2);
	}

}
