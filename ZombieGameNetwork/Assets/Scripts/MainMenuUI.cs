﻿//using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class MainMenuUI : MonoBehaviour {

    public AudioClip soulfulMusic;

    private ModalPanel[] modalPanel;

    private UnityAction myYesAction;
    private UnityAction myNoAction;
    int ModalPanelIndex = 0, AboutPanelIndex = 0, SettingsPanelIndex = 0;

    AudioSource source;

    void Awake()
    {
        AudioListener.pause = false;
        source = Camera.main.GetComponent<AudioSource>();
        source.PlayOneShot(soulfulMusic, 1F);

        modalPanel = ModalPanel.Instance();

        for(int i = 0; i < 3; ++i)
        {
            if (modalPanel[i].gameObject.name == "SomeShit")
            {
                ModalPanelIndex = i;
            }
            else if(modalPanel[i].gameObject.name == "SomeShitOne")
            {
                AboutPanelIndex = i;
            }
            else
            {
                SettingsPanelIndex = i;
            }
        }

        myYesAction = new UnityAction(YesShutdownFunction);
        myNoAction = new UnityAction(NoFunction);
    }



    public void ShotdownChoice()
    {
        modalPanel[ModalPanelIndex].Choice("Do you want to quit the game?", YesShutdownFunction, NoFunction);
    }

    public void NewGameChoice()
    {
        modalPanel[ModalPanelIndex].Choice("Do you want to start new game?", YesNewGameFunction, NoFunction);
    }

    public void SettingsChoice()
    {
        modalPanel[SettingsPanelIndex].Choice(FullScreenFunction, SoundFunction, NoFunction);
    }

    void YesShutdownFunction()
    {
        Application.Quit();
    }

    void YesNewGameFunction()
    {
        SceneManager.LoadScene("First");
    }
    
    void FullScreenFunction()
    {
        Screen.fullScreen = !Screen.fullScreen;
    }

    void SoundFunction()
    {
        AudioListener.pause = !AudioListener.pause;
    }

    void NoFunction()
    {

    }


    public void StartNewGame()
    {
        NewGameChoice();
    }

    public void Shutdown()
    {
        ShotdownChoice();
    }

    public void About()
    {
        modalPanel[AboutPanelIndex].JustPanel(NoFunction);
    }

    public void Settings()
    {

    }
}

 